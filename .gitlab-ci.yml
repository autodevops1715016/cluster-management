#--------- GitLab Agent CI/CD Tunnel configuration
# Set KUBE_CONTEXT to use with the GitLab Kubernetes Agent's CI/CD tunnel. See
# https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html.
#-----------------------------------
# variables:
# Set the variable $KUBE_CONTEXT through the GitLab UI, or set it here by
# uncommenting the following two lines and replacing the Agent's path with your own:
# variables:
#   KUBE_CONTEXT: path/to/your-agent-configuration-project:your-agent-name

.kube-context:
  before_script:
    - if [ -n "$KUBE_CONTEXT" ]; then kubectl config use-context "$KUBE_CONTEXT"; fi

# -------- Helm v2 release detection
# The detect-helm2-release will try to identify if you have any app installed with Helm v2.
# If so, it will fail the pipeline and ask you to migrate those apps to Helm v3, since
# the gl-helmfile utility will be using Helm v3 to manage your apps.
#
# If this job succeeds, it means you don't have any Helm v2 apps, so you can completely remove
# the detect-helm2-releases job from your pipeline.
#
# By default, the gl-fail-if-helm2-releases-exist utility script is checking for apps possibly
# installed in the gitlab-managed-apps namespace. If you were using a different namespace, feel
# free to replace gitlab-managed-apps by whatever that is in the script below.
#-----------------------------------

detect-helm2-releases:
  extends: [.kube-context]
  stage: test
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v1.3.2"
  environment:
    name: production
  script:
    - gl-fail-if-helm2-releases-exist gitlab-managed-apps
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

apply:
  extends: [.kube-context]
  stage: deploy
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v1.3.2"
  environment:
    name: production
  script:
    - gl-ensure-namespace gitlab-managed-apps
    - gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml apply --suppress-secrets
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# -------- Automatic package upgrades
# Uses https://gitlab.com/renovate-bot/renovate-runner to update the packages.
#
# The automtic updates can be configured using the renovate.json file. See the Renovate 
# documentation for the available options, https://docs.renovatebot.com/configuration-options/
# 
# To get started:
# 
# 1. Set the CI/CD variable `RENOVATE_TOKEN` to an access token with `api` and `read_repository` 
#    permissions (for example a Personal Access Token, a Project Access Token, or a Group Access Token).
# 2. Define a pipeline schedule. 
# 3. Optional: Trigger the new schedule to run the job right away.
#
# When the renovate job runs, a merge request is created for each available update.
# -------------------------------------

include:
  - project: 'renovate-bot/renovate-runner'
    file: '/templates/renovate-dind.gitlab-ci.yml'

renovate:
  variables:
    RENOVATE_EXTRA_FLAGS: '$CI_PROJECT_PATH'
  rules:
    - if: '$RENOVATE_TOKEN == null || $RENOVATE_TOKEN == ""'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_JOB_MANUAL == "true"'
